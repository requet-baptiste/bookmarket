package net.tncy.bre.bookmarket.services;

import net.tncy.bre.bookmarket.data.Book;
import net.tncy.bre.bookmarket.data.Bookstore;
import net.tncy.bre.bookmarket.data.InventoryEntry;

import java.util.ArrayList;
import java.util.Objects;

public class BookstoreService {

    private final ArrayList<Bookstore> bookstores = new ArrayList<>();


    // Singleton design pattern
    private static BookstoreService instance;

    private BookstoreService() {
        // The following code emulates slow initialization.
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public static BookstoreService getInstance() {
        if (instance == null) {
            instance = new BookstoreService();
        }
        return instance;
    }

    public int createBookstore(String name) {
        int id = 0;
        for (Bookstore bookstore : bookstores) {
            if (bookstore.getId() != id) {
                break;
            }
            id++;
        }
        bookstores.add(new Bookstore(id, name));
        return id;
    }

    public void deleteBookstore(int bookstoreId) {
        bookstores.removeIf(bookstore -> bookstore.getId() == bookstoreId);
    }

    public Bookstore getBookstoreById(int bookstoreId) {
        for (Bookstore bookstore : bookstores) {
            if (bookstore.getId() == bookstoreId) {
                return bookstore;
            }
        }
        return null;
    }

    public ArrayList<Bookstore> findBookstoreByName(String name) {
        ArrayList<Bookstore> f_books = new ArrayList<>();
        for (Bookstore bookstore : bookstores) {
            if (Objects.equals(bookstore.getName(), name)) {
                f_books.add(bookstore);
            }
        }
        return f_books;
    }

    public void renameBookstore(int bookstoreId, String newName) {
        for (Bookstore bookstore : bookstores) {
            if (bookstore.getId() == bookstoreId) {
                bookstore.setName(newName);
            }
        }
    }

    public void addBookToBookstore(int bookstoreId, int bookId, int qty, float price) {
        for (Bookstore bookstore : bookstores) {
            if (bookstore.getId() == bookstoreId) {
                Book book = BookService.getInstance().getBookById(bookId);
                bookstore.addToInvetoryEntries(new InventoryEntry(book, qty, price, price));
            }
        }
    }

    public void removeBookFromBookstore(int bookstoreId, int bookid) {
        for (Bookstore bookstore : bookstores) {
            if (bookstore.getId() == bookstoreId) {
                bookstore.getInventoryEntries().removeIf(inventoryEntry -> inventoryEntry.getBook().getId() == bookid);
            }
        }
    }

    public ArrayList<InventoryEntry> getBookstoreInventory(int bookstoreId) {
        for (Bookstore bookstore : bookstores) {
            if (bookstore.getId() == bookstoreId) {
                return bookstore.getInventoryEntries();
            }
        }
        return null;
    }

    public ArrayList<Book> getBookstoreCatalog() {
        ArrayList<Book> books = new ArrayList<>();
        for (Bookstore bookstore : bookstores) {
            for (InventoryEntry inventoryEntry : bookstore.getInventoryEntries()) {
                books.add(inventoryEntry.getBook());
            }
        }
        return books;
    }


}
