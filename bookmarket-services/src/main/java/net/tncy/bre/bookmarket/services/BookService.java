package net.tncy.bre.bookmarket.services;

import net.tncy.bre.bookmarket.data.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class BookService {
    private final ArrayList<Book> books = new ArrayList<>();

    // Singleton design pattern
    private static BookService instance;

    private BookService() {
        // The following code emulates slow initialization.
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public static BookService getInstance() {
        if (instance == null) {
            instance = new BookService();
        }
        return instance;
    }

    public int createBook(String title, String author, String publisher, String ISBN) {
        int id = 0;
        for (Book book : books) {
            if (book.getId() != id) {
                break;
            }
            id++;
        }
        books.add(new Book(id, title, author, publisher, BookFormat.BROCHE, ISBN));
        return id;
    }

    public void deleteBook(int bookId) {
        books.removeIf(book -> book.getId() == bookId);
    }

    public Book getBookById(int bookId) {
        for (Book book : books) {
            if (book.getId() == bookId) {
                return book;
            }
        }
        return null;
    }

    public ArrayList<Book> findBookByTitle(String title) {
        ArrayList<Book> f_books = new ArrayList<>();
        for (Book book : books) {
            if (Objects.equals(book.getTitle(), title)) {
                f_books.add(book);
            }
        }
        return f_books;
    }

    public ArrayList<Book> findBookByAuthor(String author) {
        ArrayList<Book> f_books = new ArrayList<>();
        for (Book book : books) {
            if (Objects.equals(book.getAuthor(), author)) {
                f_books.add(book);
            }
        }
        return f_books;
    }

    public Book getBookByISBN(String isbn) {
        for (Book book : books) {
            if (Objects.equals(book.getISBN(), isbn)) {
                return book;
            }
        }
        return null;
    }
}
