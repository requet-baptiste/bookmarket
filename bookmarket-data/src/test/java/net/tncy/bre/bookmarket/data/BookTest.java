package net.tncy.bre.bookmarket.data;



import org.junit.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BookTest {

    private static Validator validator;


    @BeforeClass
    public static void setUpClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validateWelldefinedISBN() {
        Book book = new Book(4556, "Le temps des tempêtes", "Sarko", "Sarko pub", BookFormat.POCHE, "978-0-596-52068-7");
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(book);
        assertEquals(0, constraintViolations.size());
    }

//    @Test
//    public void unvalidateNotWelldefinedISBN() {
//        Book book = new Book(4556, "Le temps des tempêtes", "Sarko", "Sarko pub", BookFormat.POCHE, "978-1-44-909-aa");
//        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(book);
//        for (ConstraintViolation<Book> constraintViolation: constraintViolations) {
//            System.out.println("CV : " + constraintViolation);
//        }
//        assertNotEquals(0, constraintViolations.size());
//    }

}
