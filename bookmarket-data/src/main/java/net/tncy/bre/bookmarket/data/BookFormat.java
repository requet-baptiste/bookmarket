package net.tncy.bre.bookmarket.data;

public enum BookFormat {
    BROCHE,
    POCHE
}
