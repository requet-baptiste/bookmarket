package net.tncy.bre.bookmarket.data;

import net.tncy.bre.validator.ISBN;

public class Book {

    private int id;
    private String title;
    private String author;
    private String publisher;
    private BookFormat format;

    @ISBN()
    private String ISBN;

    public Book(int id, String title, String author, String publisher, BookFormat format, String ISBN) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.format = format;
        this.ISBN = ISBN;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public BookFormat getFormat() {
        return format;
    }

    public void setFormat(BookFormat format) {
        this.format = format;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
}
